<CsoundSynthesizer>
<CsOptions>
</CsOptions>
<CsInstruments>
sr = 44100
kr = 4410
ksmps = 10
nchnls = 1
;===============================================================================
instr 1 
 kVary expseg p5, p3/2, p6, p3/2, p5; vary gap durations between maximum and minimum
 kRandom rand .5, p7; random number series +0.5~-0.5, seed = p7
 kVary init p5; begin with maximum gap

 Start:; start of reinit 
 iRandom = 0.5 + i(kRandom); 0 ~ 1
 iGap = p6 + iRandom * i(kVary); (.035 ~ (.035 + kVary))
 kBandWidth = 111 + (iRandom * 100); filter bandwidth variation
 timout 0, iGap, Continue; skip reinit for iGap (seconds)
 reinit Start
 Continue:; envelope
 aGate expseg .0001, .00777, 1, .08888, .0001
 rireturn; end of reinit

 aNoise rand p4; white noise
 aSignal reson aNoise, 333, kBandWidth, 2; bandpass filter
 /*
 The last value "2" raises the response factor so that its overall RMS value equals 1.
 This intended equalization of input and output power assumes all frequencies are 
    physically present; hence it is most applicable to white noise.
 */
 aSignal = aSignal * aGate; apply envelope
 out aSignal
 endin
</CsInstruments>
<CsScore>
;  Start dur amp    maxvary minvary seed
i1  0     44  18000  7       .077   .77225
</CsScore>
</CsoundSynthesizer>