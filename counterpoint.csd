<CsoundSynthesizer>
<CsOptions>
</CsOptions>
<CsInstruments>
sr = 44100
kr = 4410
ksmps = 10
nchnls = 2
gaReverb init 0
;===============================================================================
instr 1; Counterpoint
 iAmplitude = 0.5
 kEnvelope expon 9999, p3, 0.77
 aSignal oscil kEnvelope, cpspch(p4), 1
 aOutput = aSignal * iAmplitude
 outs1 aOutput
 gaReverb = gaReverb + (aOutput * 0.45)
 endin

instr 2; Cantus Firmus
 iAmplitude = 0.9
 iFrequency = cpspch(p4)
 kEnvelope linen 1, p3 * 0.2, p3, p3 * 0.3
 aOscillator oscil 7777, iFrequency, 2
 aSignal = aOscillator * kEnvelope
 aOutput = aSignal * iAmplitude
 outs2 aOutput
 gaReverb = gaReverb + (aOutput * 0.2) 
 endin

instr 3; Reverberation
 iReverb_Time = p4
 aSignal nreverb gaReverb, iReverb_Time, 0.4
 outs aSignal, aSignal
 gaReverb = 0
 endin

instr 4; Granular Synthesis Textures
 iAmplitude = .1
 ; ares  grain xamp, xpitch, xdens, kampoff, kpitchoff, kgdur, igfn, iwfn, imgdur 
 aSignal grain 2000, 220,    1000,  0,       20000,     0.05,  3,    4,    1
 aOutput = aSignal * iAmplitude
 outs aOutput, aOutput
 endin

instr 5; Formant
 iAmplitude = .6
 aCarrier line 392, p3, 800
 index = 2.0
 iModFreq = 392
 idev = index * iModFreq
 aModSig oscil idev, iModFreq, 1
 ; ares  fof xamp, xfund, xform,              koct, kband, kris,  kdur, kdec, iolaps, ifna, ifnb, itotdur, iphs, ifmode
 aSignal fof 5000, 5,     aCarrier + aModSig, 0,    1,     0.003, 0.5,  0.1,  3,      5,    6,    p3,      0,    1
 aOutput = aSignal * iAmplitude
 outs aOutput, aOutput
 endin
</CsInstruments>
<CsScore>
; instr 1 and 2
 f 1 0 8192 10 10 3 2 .9 .7  .4  .2 .1  .02 .005 .0007 .0003
 f 2 0 8192 10 10 1 .5   .33 .25 .2 .17 .14 .12  .11   .1
; Granular Synthesis Textures
 f 3 0 8192 10 1
 f 4 0 1024 20 2 1
; Formant
 f 5 0 4096 10 1
 f 6 0 1024 19 .5 .5 270 .5
;===============================================================================
; Counterpoint
 i1  1.5  0.5   7.02; D
 i1  +    0.5   7.00; C
 i1  +    0.5   7.02; D
 i1  +    0.75  7.03; Eb
 i1  +    0.25  7.02; D
 i1  +    0.25  7.03; Eb
 i1  +    0.25  7.05; F
 i1  +    0.25  7.07; G
 i1  +    0.25  7.09; A
 i1  +    0.25  7.10; Bb
 i1  +    0.25  7.09; A
 i1  +    1     7.07; G
 i1  +    0.25  7.05; F
 i1  +    0.25  7.07; G
 i1  +    0.25  7.09; A
 i1  +    0.25  7.10; Bb
 i1  +    0.25  8.00; C
 i1  +    0.25  7.09; A
 i1  +    0.5   8.02; D
 i1  +    0.75  7.07; G
 i1  +    0.125 7.06; F#
 i1  +    0.125 7.04; E
 i1  +    0.5   7.06; F#
 i1  +    1     7.07; G
; Cantus Firmus
 i2 0 1 6.07; G
 i2 + . 6.10; Bb
 i2 + . 6.09; A
 i2 + . 6.07; G
 i2 + . 7.00; C
 i2 + . 6.10; Bb
 i2 + . 7.02; D
 i2 + . 7.00; C
 i2 + . 6.10; Bb
 i2 + . 6.09; A
 i2 + . 6.07; G
; Reverberation
 i3 0 13 3.77
; Granular Synthesis Textures
 i4  0.0 1.1
 i4  8.0 0.5
 i4 10.0 1.0
; Formant
 i5 8 3
; Tempo
 t 0 80 7 70 8 60 10 50
 /*
 t  p1  p2  p3  p4 ... (unlimited)
 p1: Must be zero. 
 p2: Initial tempo on beats per minute. 
 p3, p5, p7: Times in beats per minute (in non-decreasing order). 
 p4, p6, p8: Tempi for the referenced beat times. 
 */
</CsScore>
</CsoundSynthesizer>