<CsoundSynthesizer>
<CsOptions>
</CsOptions>
<CsInstruments>
sr = 44100
kr = 4410
ksmps = 10
nchnls = 2
;===============================================================================
instr 1
 iAmplitude1 = p4
 iAmplitude2 = p4 * 0.00001
 iAmplitude3 = p4 * 0.5   
 iDuration1 = p3 * p9 ; portion of Part3 for Part1
 iDuration2 = p3 * p10; portion of Part3 for Part2
 iDuration3 = p3 - (iDuration2 + iDuration1); the rest to Part3
 iCPS1_a = cpspch(p5)
 iCPS1_b = iCPS1_a * 2
 iCPS3 = cpspch(p6)
 iSeed1 = p7
 ;iSeed2 = p8
 iMinDuration = p11; min note length for Part2
 iMaxNumber = p12; max gap factor (>= 1)
 iOctaveRange = p13; random pitch range

 iPanFunciton = p14; pan control function
 kPan oscil1i 0, 1, p3, iPanFunciton; overall pan control
 kLeft = sqrt(kPan); phrase
 kRight = sqrt(1 - kPan)

 iWave1 = 1; osc waveshape for Part1
 iWave3 = 2; osc waveshape for Part3
 iHamm = 3; hamming function number for LFO
;-------------------------------------------------------------------------------
Part1:; cresc. to Part2
 timout iDuration1, p3, Part2; wait iDuration1 seconds
 kEvnelope1 expseg 1, iDuration1, iAmplitude1, 1, iAmplitude1
 kLFO1 oscil 1, 7, iHamm 
 kGate randi 0.5, 5; output between +0.5~-0.5
 kGate = 0.5 + kGate; offset to between 0~1
 aSignal_a oscili   kGate, iCPS1_a, iWave1
 aSignal_b oscili 1-kGate, iCPS1_b, iWave1
 aSignal1 = (aSignal_a + aSignal_b) * kEvnelope1 * kLFO1   
 outs (aSignal1 * kLeft), (aSignal1 * kRight)
 kgoto End
;-------------------------------------------------------------------------------
Part2:; granulator
 timout iDuration2, p3, Part3
 ; ares   grain xamp, xpitch, xdens, kampoff, kpitchoff, kgdur, igfn, iwfn, imgdur 
 aSignal2 grain 2000, 220,    1000,  0,       20000,     .05,   8,    9,    1
 aSignal2 = aSignal2 * iAmplitude2
 outs (aSignal2 * kLeft), (aSignal2 * kRight)
 kgoto End
;-------------------------------------------------------------------------------
Part3:;dim. and rit.
 kEnvelope3 line 1, iDuration3, 0; linear decay
 kLFO_Hz expon 20, iDuration3, 1; exp decay for LFO rate
 kLFO3 oscil 1, kLFO_Hz, iHamm; hamming window for LFO
 aSignal3 oscili kEnvelope3 * iAmplitude3, iCPS3, iWave3
 outs (aSignal3 * kLeft * kLFO3), (aSignal3 * kRight * (1-kLFO3))
;-------------------------------------------------------------------------------
End:
 endin
</CsInstruments>
<CsScore>
f 1 0 8192 10 1;iWave1
f 2 0 8192 10 1  0   .25 0 .125;iWave3
f 3 0 8192 20 1;hamming window function for LFO gating
f 4 0 128  7  .5 3   .5;fixed center panning function (.5 constant)
f 5 0 128  7  1  129 0;pan L to R
f 6 0 128  7  0  129 1;pan R to L
f 7 0 128  7  .5 32  1   64 0   33 .5;pan C to L to R to C
f 8 0 8192 10 1   ; granular synthesis textures
f 9 0 1024 20 2  1; granular synthesis textures
;===============================================================================
;        Amplitude Pitch1 Pitch3 Seeds Dur Portion Min Max Octave Pan
; 1 2 3  4         5      6      7  8  9   10      11  12  13     14
i 1 0 11 55555     8.04   6.09   .9 .2 .4  .025    .04 3   2      4
i 1 3 11 33333     9.03   7.04   .8 .3 .3  .05     .05 5   3      7
i 1 6 11 66666     7.05   7.11   .7 .4 .2  .1      .06 7   4      6
i 1 7 11 44444     8.07   7.03   .6 .5 .3  .1      .07 4   3      5
</CsScore>
</CsoundSynthesizer>