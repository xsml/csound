<CsoundSynthesizer>
<CsOptions>
</CsOptions>
<CsInstruments>
sr = 44100
kr = 4410
ksmps = 10
nchnls = 2
;===============================================================================
instr 1
 iDynamics = p4
 iPitchFn = p5
 iDurationFn = p6
 iAmplitudeFn = p7
 iPhraseRise = p8; envelope rise time
 iPhraseDecay = p9; envelope decay time
 iNoteRise = p10; amplitude rise
 iNoteDecay = p11; amplitude decay
 iSeed1 = p12
 iSeed2 = p13
 iSeed3 = p14

 iPkDuration = 0.5001; max expected duration
 kPhrase expseg 0.001, iPhraseRise, 1, p3 - iPhraseRise - iPhraseDecay, 1, iPhraseDecay, 0.001; phrase envelope
 kPitchIndex rand 0.5, iSeed1
 kDurationIndex rand 0.5, iSeed2; table indices -.5 ~ +.5
 kPan rand 0.5, iSeed3
;-------------------------------------------------------------------------------
Noteinit:; start reinit block
 iDurationIndex = 0.5 + i(kDurationIndex); 0 ~ 1
 iDuration table iDurationIndex, iDurationFn, 1; select from normalized duration table
 iAmplitude table iDuration / iPkDuration, iAmplitudeFn, 1; relate amplitude to normalized duration
 iAmplitude = 0.25 + (0.75 * iAmplitude); amplitude range from .25 to 1
 iPitchIndex = 0.5 + i(kPitchIndex); make a positive i-variable
 iPitch table iPitchIndex, iPitchFn, 1; select from pitch table
 iCPS = (iPitch == 0 ? 0 : cpspch(iPitch)); allow for 0s (rests)

 iPan = i(kPan) + 0.5; 0 ~ 1
 iLeft = sqrt(iPan); fill the hole between two speakers
 iRight = sqrt(1 - iPan)
 if (iCPS == 0) goto Rest; a table value of 0 = a rest

 iRise = iNoteRise * iDuration; note rise time
 iDecay = iNoteDecay * iDuration; note decay time
 iSustain = iDuration - iRise - iDecay; sustain portion
 kNote expseg 0.001, iRise, 1, iSustain, 1, iDecay, 0.001
 aSignal pluck kNote * iAmplitude * iDynamics, iCPS, iCPS, 0, 1, 0, 0
 aSignal = aSignal * kPhrase
 outs aSignal * iLeft, aSignal * iRight
;-------------------------------------------------------------------------------
Rest:
 timout 0, iDuration, End; timout can be anywhere
 reinit Noteinit; in the reinit block
;-------------------------------------------------------------------------------
End:
 endin
</CsInstruments>
<CsScore>
;Pitch Function 1
 f 1 0 8  -2 4.11 5.11 6.0985 7.0525 7.1115 8.0245 8.0475 8.0915
 f 2 0 32 -2 10.09   10.09   10.09  10.09  10.09   11.09   11.09   11.09
  9.09    9.09   10.095 10.095 11.095  10.095  10.09   10.085
  9.085   9.085  11.085 11.095 10.0925 10.0925 10.0925 10.0875
 10.0875 10.0875;micro-tones with 6 rests
 f 3 0 8  -2 6.03 6.03 6.03 6.05 6.05 6.09 6.09; with 1 rest
;Duration Function: 1 = whole note
 f 4 0 2 -2 .25 .125
 f 5 0 8 -2 .5 .25 .25 .125 .125 .0625 .0625 .03125
 f 6 0 4 -2 .25 .25 .125 .125
;Amplitude Function: quarter sine wave
 f 7 0 128 9 .25 1 0
;===============================================================================
;                  Function:_______________ Phrase:___ Note:_____ Seeds:_____________
;         Dynamics Pitch Duration Amplitude Rise Decay Rise Decay Pitch Duration Pan
; 1 2  3  4        5     6        7         8    9     10   11    12    13       14
i 1 0  80 10000    1     4        7         .01  3     .1   .2    11    12       13
i 1 10 65 20000    1     4        7         .01  3     .1   .2    21    22       23
i 1 20 20 30000    2     4        7         2    3     .01  .1    31    32       33
i 1 30 15 50000    2     5        7         2    3     .01  .1    41    22       43
i 1 40 25 40000    3     6        7         2    3     .2   .3    51    52       53
i 1 60 10 70000    2     6        7         2    3     .01  .1    61    62       63
</CsScore>
</CsoundSynthesizer>